package aos

import (
	"testing"
	"github.com/stretchr/testify/assert"
)

type Example struct {
	example string
}

func TestSet(t *testing.T) {
	ContainerSet("example", Example{"example"})
	example := ContainerGet("example")
	assert.Equal(t, "example", example.(Example).example)

	ok := ContainerHas("id")
	assert.Equal(t, false, ok)
}